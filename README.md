# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version - 2.7.3

* Rails version - 6.1.3.2

* System dependencies:
  - gem video_info
  - bootstrap v5
  - fontawesome v5

* Configuration
  - youtube api credentail key

* Database
  - postgresql

* How to run the test suite
  ```
  bin/rspec spec
  ```
* Development instructions
  ```
  bundle install
  rake db:create
  rake db:migrate
  npm install
  rails s
  ```

* Deployment instructions
  ```
  heroku login
  heroku create
  heroku run rake --trace db:migrate
  ```

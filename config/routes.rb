Rails.application.routes.draw do
  root to: 'movies#index'

  post    :login_or_register, to: 'sessions#create'
  delete  :logout, to: 'sessions#destroy'

  resources :movies, only: [:create, :destroy]
  get 'share' => 'movies#new', as: :share_movie
end

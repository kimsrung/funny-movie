require 'rails_helper'

RSpec.describe Movie, type: :model do
  describe 'associations' do
    it { should belong_to(:user) }
  end

  describe 'validations' do
    it { should validate_presence_of(:url) }
  end

  describe 'callbacks' do
    it 'set movie title and description before create' do
      user = User.create(username: 'kimsrung', password: '123456')
      movie = Movie.new(url: 'https://youtu.be/qP8kir2GUgo', user: user)
      movie.save

      movie.reload
      expect(movie.title).to eq 'GitLab CI CD Tutorial for Beginners [Crash Course]'
    end
  end

  describe 'methods' do
    it 'should have embed_url' do
      movie = Movie.new(url: 'https://www.youtube.com/watch?v=VZTdl0J_4Nk')

      expect(movie.embed_url).to eq 'https://www.youtube.com/embed/VZTdl0J_4Nk'
    end
  end
end

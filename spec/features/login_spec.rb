require 'rails_helper'

RSpec.describe 'Login', type: :feature do
  scenario 'valid' do
    User.create(username: 'kimsrung', password: '123abc')

    visit root_path
    fill_in('username',	with: 'kimsrung')
    fill_in('password',	with: '123abc')
    click_button('Login / Register')
    expect(page).to have_content('Login successfully')
  end

  scenario 'invalid' do
    User.create(username: 'kimsrung', password: '123abc')

    visit root_path
    fill_in('username',	with: 'kimsrung')
    fill_in('password',	with: 'wrong_pwd')
    click_button('Login / Register')
    expect(page).to have_content('Fail to login')
  end
end

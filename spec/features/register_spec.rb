require 'rails_helper'

RSpec.describe 'Register', type: :feature do
  scenario 'valid' do
    visit root_path
    fill_in('username',	with: 'kimsrung')
    fill_in('password',	with: '123abc')
    click_button('Login / Register')
    expect(page).to have_content('Register successfully')
  end

  scenario 'invalid' do
    visit root_path
    fill_in('username',	with: 'kimsrung')
    fill_in('password',	with: 'less')
    click_button('Login / Register')
    expect(page).to have_content('Fail to register')
  end
end

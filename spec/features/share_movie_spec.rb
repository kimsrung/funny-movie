require 'rails_helper'

RSpec.describe 'Share movie', type: :feature do
  scenario 'invalid' do
    visit share_movie_path

    expect(page).to have_current_path(root_path)
    expect(page).to have_content('Require login or register')
  end

  scenario 'invalid url' do
    visit root_path
    fill_in('username',	with: 'kimsrung')
    fill_in('password',	with: '123456')
    click_button('Login / Register')
    click_link('Share a movie')
    fill_in('movie_url',	with: '')
    click_button('Share')
    expect(page).to have_content("Url can't be blank")
  end

  scenario 'valid url' do
    visit root_path
    fill_in('username',	with: 'kimsrung')
    fill_in('password',	with: '123456')
    click_button('Login / Register')
    click_link('Share a movie')
    fill_in('movie_url',	with: 'https://youtu.be/qP8kir2GUgo')
    click_button('Share')
    expect(page).to have_content('Share successfully')
  end
end

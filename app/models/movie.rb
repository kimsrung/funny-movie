class Movie < ApplicationRecord
  belongs_to :user

  validates :url, presence: true

  before_create :set_title_description

  def embed_url
    url.gsub('watch?v=', 'embed/')
  end

  private

  def set_title_description
    info = VideoInfo.new(url)
    self.title = info.title
    self.description = info.description
  end
end

class Reverse
  attr_accessor :items

  def initialize
    @items = []
  end

  def add(val)
    return if check_exist(val)

    items << val
  end

  def remove(val)
    items.delete(val)
  end

  def check_exist(val)
    items.any? val
  end
end

class MoviesController < ApplicationController
  before_action :require_login!, except: :index

  def index
    @movies = Movie.order(id: :desc)
  end

  def new
    @movie = Movie.new
  end

  def create
    @movie = current_user.movies.build(movie_params)

    if @movie.save
      flash[:success] = 'Share successfully'
      alert_telegram(@movie)
      redirect_to root_path
    else
      render :new
    end
  end

  def destroy
    @movie = current_user.movies.find(params[:id])
    @movie.destroy

    flash[:success] = 'Delete Successfully'
    redirect_to root_path, status: :ok
  end

  private

  def movie_params
    params.require(:movie).permit(:url)
  end

  def require_login!
    return if logged_in?

    flash[:notice] = 'Require login or register'
    redirect_to root_path
  end

  def alert_telegram(movie)
    message = "You have a new movie added. It called '#{movie.title}'"
    uri = URI("#{TELEGRAM_API_URL}/#{TELEGRAM_BOT_TOKEN}/sendMessage?chat_id=#{TELEGRAM_CHAT_ID}&text=#{message}")

    Net::HTTP.get(uri)
  end
end

class SessionsController < ApplicationController
  def create
    @user = User.find_or_initialize_by(username: user_params[:username])

    if @user.new_record?
      create_user
    else
      login_user
    end

    redirect_to root_path
  end

  def destroy
    session[:user_id] = nil
    flash[:success] = 'Logout successfully'

    redirect_to root_path
  end

  private

  def user_params
    params.require(:user).permit(:username, :password)
  end

  def create_user
    @user.password = user_params[:password]
    if @user.save
      flash[:success] = 'Register successfully'
      session[:user_id] = @user.id
    else
      flash[:error] = "Fail to register: #{error_message}"
    end
  end

  def login_user
    if @user.authenticate(user_params[:password])
      flash[:success] = 'Login successfully'
      session[:user_id] = @user.id
    else
      flash[:error] = 'Fail to login'
    end
  end

  def error_message
    @user.errors.full_messages.join(', ')
  end
end

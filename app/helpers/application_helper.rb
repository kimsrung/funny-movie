module ApplicationHelper
  def alert_class_name(flash_type)
    case flash_type.to_sym
    when :error
      'danger'
    when :alert
      'warning'
    when :notice
      'info'
    else
      'success'
    end
  end
end

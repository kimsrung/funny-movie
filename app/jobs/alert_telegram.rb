class AlertTelegram
  include Sidekiq::Worker

  def perform(*args)
    message = "Total movies: '#{Movie.count}'"
    uri = URI("#{TELEGRAM_API_URL}/#{TELEGRAM_BOT_TOKEN}/sendMessage?chat_id=#{TELEGRAM_CHAT_ID}&text=#{message}")

    Net::HTTP.get(uri)
  end
end

Sidekiq::Cron::Job.create(name: 'Hard worker - every 1min', cron: '1 * * * *', class: 'AlertTelegram')